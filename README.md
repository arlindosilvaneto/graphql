# GraphQL Boilerplate for PostgreSQL

This project intents to provide a common layer to GraphQL projects.

## Architecture

![Project Arquitecture](./docs/architecture.png)

## GraphQL Language Schema

```
[ [ query ] | mutation ] [ <name> [ ( [ var:type]* ) ] ] {
	[ alias: ] [ <method> | <collection> ] [ ( [ param:[value | var] ]* ) ] {
		[ [ alias: ] <field> [ { 
			[ alias : ] <field> 
		} ] | <fragment> ]+
		[ <fragment> ]*
	}
}

[ fragment <name> on <model> {
	[ alias: ] <field> [ { 
		[ alias : ] <field> 
	} ]+ 
} ]*

* => zero or more
+ => one or more
```

## Project Requirements

1. Mininal knowledge in NodeJS and Express.
2. PostgreSQL Server (version 9.5 - use docker image postgres:9.5 for tests).

## Running the Project

1. Install all local dependencies: npm install
2. Create test database
3. Restore test database from backup file on the project
4. Update app/sql.js file with the correct database connection string for your database server and its credentials.
2. Run the default npm task: npm start
3. Access [http://localhost:3000/graphql](http://localhost:3000/graphql) for GraphiQL Utility.

## Authentication

The project uses a low level basic authentication defaulting to:

- user: aisoft
- password: aisoft