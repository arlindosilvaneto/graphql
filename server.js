import Express from 'express';
import GraphQLHTTP from 'express-graphql';
import Schema from './app/schema';
import Auth from './app/auth';

// Config
const APP_PORT = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'dev';

const app = Express();

app.use('/graphql', Auth.basic, GraphQLHTTP({
	schema: Schema,
	pretty: true,
	graphiql: (env === 'dev')
}));

app.listen(APP_PORT, () => {
	console.log('Listening on ' + APP_PORT);
});