import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull
} from 'graphql';

import Post from '../models/post';
import PostResolves from '../resolvers/postResolvers';

const PostQuery = {
	getPosts: {
		type: new GraphQLList(Post),
		args: {
			id: {
				type: GraphQLInt
			},
			title: {
				type: GraphQLString
			}
		},
		resolve(root, args) {
			return PostResolves.getPosts(args);
		}
	}
};

export default PostQuery;