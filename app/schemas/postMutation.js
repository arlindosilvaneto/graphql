import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull
} from 'graphql';

import PostResolves from '../resolvers/postResolvers';
import Post from '../models/post';

const PostMutation = {
	addPost: {
		type: Post,
		args: {
			title: {
				type: new GraphQLNonNull(GraphQLString)
			},
			content: {
				type: new GraphQLNonNull(GraphQLString)
			},
			personId: {
				type: new GraphQLNonNull(GraphQLInt)
			}
		},
		resolve(root, args) {
			return PostResolves.addPost(args);
		}
	},
};

export default PostMutation;