import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull
} from 'graphql';

import Profile from '../models/profile';
import ProfileResolves from '../resolvers/profileResolvers';

const ProfileQuery = {
	
};

export default ProfileQuery;