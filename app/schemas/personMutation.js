import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull
} from 'graphql';

import Person from '../models/person';
import PersonResolves from '../resolvers/personResolvers';

const PersonMutation = {
	addPerson: {
		type: Person,
		args: {
			firstName: {
				type: new GraphQLNonNull(GraphQLString)
			},
			lastName: {
				type: new GraphQLNonNull(GraphQLString)
			},
			email: {
				type: new GraphQLNonNull(GraphQLString)
			}
		},
		resolve(root, args) {
			return PersonResolves.addPerson(args);
		}
	},
	updatePerson: {
		type: Person,
		args: {
			id: {
				type: new GraphQLNonNull(GraphQLInt)
			},
			firstName: {
				type: GraphQLString
			},
			lastName: {
				type: GraphQLString
			},
			email: {
				type: GraphQLString
			}
		},
		resolve(root, args) {
			return PersonResolves.updatePerson(args);
		}
	},
	deletePerson: {
		type: Person,
		args: {
			id: {
				type: new GraphQLNonNull(GraphQLInt)
			}
		},
		resolve(root, args) {
			return PersonResolves.deletePerson(args);
		}
	}
};

export default PersonMutation;