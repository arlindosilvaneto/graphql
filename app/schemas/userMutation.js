import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull
} from 'graphql';

import Person from '../models/user';
import PersonResolves from '../resolvers/userResolvers';

const PersonMutation = {
	
};

export default PersonMutation;