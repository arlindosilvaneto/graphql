import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull
} from 'graphql';

import User from '../models/user';
import UserResolves from '../resolvers/UserResolvers';

const UserQuery = {
	getUser: {
		type: new GraphQLList(User),
		args: {
			id: {
				type: GraphQLInt
			},
			provider: {
				type: GraphQLString
			},
			providerId: {
				type: GraphQLString
			},
			token: {
				type: GraphQLString
			}
		},
		resolve(root, args) {
			return UserResolves.getUser(args);
		}
	}
};

export default UserQuery;