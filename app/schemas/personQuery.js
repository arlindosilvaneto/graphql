import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull
} from 'graphql';

import Person from '../models/person';
import PersonResolves from '../resolvers/personResolvers';

const PersonQuery = {
	getPeople: {
		type: new GraphQLList(Person),
		args: {
			id: {
				type: GraphQLInt
			},
			email: {
				type: GraphQLString
			}
		},
		resolve(root, args) {
			return PersonResolves.getPeople(args);
		}
	}
};

export default PersonQuery;