import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull
} from 'graphql';

import Person from '../models/person';
import PersonResolves from '../resolvers/personResolvers';

const PersonMutation = {
	
};

export default PersonMutation;