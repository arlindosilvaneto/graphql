import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList
} from 'graphql';

import UserResolver from '../resolvers/userResolvers';

const Profile = new GraphQLObjectType({
	name: 'Profile',
	description: 'This represents a user profile',
	fields: () => {
		return {
			id: {
				type: GraphQLInt,
				resolve(profile) {
					return profile.id;
				}
			},
			firstName: {
				type: GraphQLString,
				resolve(profile) {
					return profile.firstName;
				}
			},
			lastName: {
				type: GraphQLString,
				resolve(profile) {
					return profile.lastName;
				}
			},
			email: {
				type: GraphQLString,
				resolve(profile) {
					return profile.email;
				}
			},
			userId: {
				type: GraphQLInt,
				resolve(profile) {
					return UserResolver.getUser({id: profile.userId});
				}
			}
		};
	}
});

export default Profile;