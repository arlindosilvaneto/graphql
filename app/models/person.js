import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList
} from 'graphql';

import Post from './post';
import PostResolves from '../resolvers/postResolvers';

const Person = new GraphQLObjectType({
	name: 'Person',
	description: 'This represents a publisher',
	fields: () => {
		return {
			id: {
				type: GraphQLInt,
				resolve(person) {
					return person.id;
				}
			},
			firstName: {
				type: GraphQLString,
				resolve(person) {
					return person.firstName;
				}
			},
			lastName: {
				type: GraphQLString,
				resolve(person) {
					return person.lastName;
				}
			},
			email: {
				type: GraphQLString,
				resolve(person) {
					return person.email;
				}
			},
			posts: {
				type: new GraphQLList(Post),
				resolve(person) {
					return PostResolves.getPosts({personId: person.id});
				}
			}
		};
	}
});

export default Person;