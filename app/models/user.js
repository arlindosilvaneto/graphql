import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList
} from 'graphql';

const User = new GraphQLObjectType({
	name: 'User',
	description: 'This represents a user',
	fields: () => {
		return {
			id: {
				type: GraphQLInt,
				resolve(user) {
					return user.id;
				}
			},
			provider: {
				type: GraphQLString,
				resolve(user) {
					return user.provider;
				}
			},
			providerId: {
				type: GraphQLString,
				resolve(user) {
					return user.providerId;
				}
			},
			token: {
				type: GraphQLString,
				resolve(user) {
					return user.token;
				}
			}
		};
	}
});

export default User;