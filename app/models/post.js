import {
	GraphQLObjectType,
	GraphQLInt,
	GraphQLString,
	GraphQLList
} from 'graphql';

import Person from './person';

const Post = new GraphQLObjectType({
	name: 'Post',
	description: 'This represents a post',
	fields: () => {
		return {
			id: {
				type: GraphQLInt,
				resolve(post) {
					return post.id;
				}
			},
			title: {
				type: GraphQLString,
				resolve(post) {
					return post.title;
				}
			},
			content: {
				type: GraphQLString,
				resolve(post) {
					return post.content;
				}
			},
			person: {
				type: Person,
				resolve(post) {
					return post.getPerson();
				}
			}
		};
	}
});

export default Post;