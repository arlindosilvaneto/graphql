import pg from 'pg';
import Promise from 'mpromise';
import _ from 'lodash';

const connString = 'postgres://postgres:postgres@192.168.99.100/test';
//const connString = 'postgres://rltayjzc:ucWucptxehPviJGL1mAG5uU6fMVnbGRh@pellefant.db.elephantsql.com:5432/rltayjzc';

export default class Conn {

	static getWhere(args, aggregator = 'AND') {
		let where = [];

		_.each(args, (value, key) => {
			if(value) {
				let quote = (typeof value === 'string') ? "'" : "";

				where.push('"' + key + '" = ' + quote + value + quote);
			}
		});

		return (where.length ? ' WHERE ' : '') + where.join(' ' + aggregator + ' ');
	}

	static doInsert(table, args) {
		let fields = [],
			values = [];

		_.each(args, (value, key) => {
			if(value) {
				let quote = (typeof value === 'string') ? "'" : "";

				fields.push(key);
				values.push(quote + value + quote);
			}
		});

		return this.doDml('INSERT INTO ' + table + ' ("' + fields.join('", "') + '") VALUES (' + values.join(', ') + ') RETURNING *');
	}

	static doUpdate(table, args, where) {
		let set = [];

		_.each(args, (value, key) => {
			if(value) {
				let quote = (typeof value === 'string') ? "'" : "";

				set.push('"' + key + '" = ' + quote + value + quote);
			}
		});

		return this.doDml('UPDATE ' + table + ' SET ' + set.join('", "') + ' ' + where + ' RETURNING *');
	}

	static doDelete(table, where) {
		return this.doDml('DELETE FROM ' + table + where + ' RETURNING *');
	}

	static doQuery(dql, params) {
		let deferred = new Promise;

		params = params || [];

		pg.connect(connString, (err, client, done) => {
		  if(err) {
		    console.error('error fetching client from pool', err);

		    return deferred.reject(err);
		  }

		  client.query(dql, params, function(err, result) {
		    //call `done()` to release the client back to the pool
		    done();

		    if(err) {
		      console.error('error running query', err);

		      deferred.reject(err);
		    }		    

		    if(result && result.rows) {
		    	deferred.fulfill(result.rows);
		    } else if(result) {
		    	deferred.fulfill([]);
		    } else {
		    	deferred.reject('no found results');
		    }
		  });
		});

		return deferred;
	}

	static doDml(dml, params) {
		let deferred = new Promise;

		params = params || [];

		console.log(dml);

		pg.connect(connString, (err, client, done) => {
		  if(err) {
		    console.error('error fetching client from pool', err);

		    return deferred.reject(err);
		  }

		  client.query(dml, params, function(err, result) {
		    //call `done()` to release the client back to the pool
		    done();

		    if(err) {
		      console.error('error running instruction', err);

		      deferred.reject(err);
		    }		    

		    if(result && result.rows && result.rows.length) {
		    	deferred.fulfill(result.rows[0]);
		    } else if(result) {
		    	deferred.fulfill({});
		    } else {
		    	deferred.fulfill();
		    }
		  });
		});

		return deferred;
	}
}