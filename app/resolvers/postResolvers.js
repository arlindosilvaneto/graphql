import Sql from '../sql';
import Promise from 'mpromise';

export default class Resolvers {

	static getPosts(args) {
		let dql = 'select * from posts',
			query;

		query = dql + Sql.getWhere({id: args.id, title: args.title, personId: args.personId}, 'and');

		console.log(query);
		return Sql.doQuery(query);
	}

	static addPost(args) {
		return Sql.doInsert('posts', {
			title: args.title,
			content: args.content,
			personId: args.personId
		});
	}
}