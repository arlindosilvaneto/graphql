import Sql from '../sql';

export default class Resolvers {

	static getPeople(args) {
		let dql = 'select * from people',
			query;

		query = dql + Sql.getWhere({id: args.id, email: args.email}, 'and');

		console.log(query);
		return Sql.doQuery(query);
	}

	static addPerson(args) {
		return Sql.doInsert('people', {
			firstName: args.firstName,
			lastName: args.lastName,
			email: args.email.toLowerCase()
		});
	}

	static updatePerson(args) {
		return Sql.doUpdate('people', {
			firstName: args.firstName,
			lastName: args.lastName,
			email: args.email ? args.email.toLowerCase() : undefined
		}, Sql.getWhere({id: args.id}));
	}

	static deletePerson(args) {
		return Sql.doDelete('people', Sql.getWhere({id: args.id}));
	}
}