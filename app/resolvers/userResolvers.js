import Sql from '../sql';

export default class Resolvers {

	static getProfile(args) {
		let dql = 'select * from users',
			query;

		query = dql + Sql.getWhere({id: args.id, providerId: args.providerId, token: args.token});

		return Sql.doQuery(query);
	}

	static insertUser(args) {
		return Sql.doUpdate('users', {
			provider: args.provider,
			providerId: args.providerId
		});
	}

	static updateUser(args) {
		return Sql.doUpdate('users', {
			token: args.token
		}, Sql.getWhere({id: args.id}));
	}
}