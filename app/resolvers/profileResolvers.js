import Sql from '../sql';

export default class Resolvers {

	static getProfile(args) {
		let dql = 'select * from profiles',
			query;

		query = dql + Sql.getWhere({userId: args.userId});

		return Sql.doQuery(query);
	}

	static insertProfile(args) {
		return Sql.doUpdate('profiles', {
			firstName: args.firstName,
			lastName: args.lastName,
			email: args.email ? args.email.toLowerCase() : undefined,
			background: args.background,
			userId: args.userId
		});
	}

	static updateProfile(args) {
		return Sql.doUpdate('profiles', {
			firstName: args.firstName,
			lastName: args.lastName,
			email: args.email ? args.email.toLowerCase() : undefined,
			background: args.background,
		}, Sql.getWhere({userId: args.userId}));
	}
}