
import {
	GraphQLObjectType,
	GraphQLSchema
} from 'graphql';

import _ from 'lodash';

import PersonQuery from './schemas/personQuery';
import PersonMutation from './schemas/personMutation';
import PostQuery from './schemas/postQuery';
import PostMutation from './schemas/postMutation';

const Query = new GraphQLObjectType({
	name: 'Query',
	description: 'Functions to query stuff',
	fields: () => {
		return _.merge({}, PersonQuery, PostQuery);
	}
});

const Mutation = new GraphQLObjectType({
	name: 'Mutation',
	description: 'Functions to create stuff',
	fields() {
		return _.merge({}, PersonMutation, PostMutation);
	}
});

const Schema = new GraphQLSchema({
	query: Query,
	mutation: Mutation
});

export default Schema;

