
import basicAuth from 'basic-auth';

export default class Auth {

	static basic(req, res, next) {
		function unauthorized(res) {
		    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
		    return res.send(401);
		}

		let user = basicAuth(req);
		let admin = Auth.getAdmin();

		if (!user || !user.name || !user.pass) {
		    return unauthorized(res);
		}

		if (user.name === admin.user && user.pass === admin.password) {
		    return next();
		} else {
		    return unauthorized(res);
		}
	}

	static getAdmin() {
		return {
			user: 'aisoft',
			password: 'aisoft'
		};
	}
}